const request = require('request')

const OPEN_WEATHER_KEY = '3d9ceaea9f52340b69982b8a619fbe09';
const units = 'f'

const forecast = (latitude, longitude, callback) => {
    let url = 'https://api.openweathermap.org/data/2.5/weather?lat=' + latitude + '&lon=' + longitude + '&appid=' + OPEN_WEATHER_KEY;

    url += '&units=imperial';
    

    request({ url, json: true }, (error, { body }) => {
        if (error) {
            callback('Unable to connect to weather service!', undefined)
        } else if (body.error) {
            callback(`Unable to find location ${body}`, undefined)
        } else {
            callback(undefined, `Temp is ${body.main.temp}. Feels like ${body.main.feels_like}. Wind is ${body.wind.speed}.`)
        }
    })
}

module.exports = forecast